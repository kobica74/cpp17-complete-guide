#include <iostream>
#include <experimental/memory_resource>

#include "TrackNew.hpp"

int main() {
    {
        TrackNew::reset();
        TrackNew::trace(true);
        std::string s = "string value with 26 chars";
        auto p1 = new std::string{"an initial value with even 35 chars"};
        auto p2 = new(std::align_val_t{64}) std::string[4];
        auto p3 = new std::string[4]{"7 chars", "x", "or 11 chars",
                                     "a string value with 28 chars"};
        TrackNew::status();
        //...
        delete p1;
        delete[] p2;
        delete[] p3;
    }

    {
        TrackNew::reset();

        // allocate some memory on the stack:
        std::array<std::byte, 200000> buf;

        // and use it as initial memory pool for a vector:
        std::experimental::pmr:: monotonic_buffer_resource pool{buf.data(), buf.size()};
        std::experimental::pmr::vector<std::string> coll{&pool};

        for (int i=0; i < 1000; ++i) {
            coll.emplace_back("just a non-SSO string");
        }

        TrackNew::status();
    }

    return 0;
}